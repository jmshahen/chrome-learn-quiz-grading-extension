// Saves options to chrome.storage.sync.
function save_options() {
    var debug = document.getElementById('debug').checked;
    var showBox = document.getElementById('showBox').checked;
    var num_graded = document.getElementById('num_graded').value;
    var num_total = document.getElementById('num_total').value;
    var font_size = document.getElementById('font_size').value;
    chrome.storage.sync.set({
        debug: debug,
        showBox: showBox,
        num_graded: parseInt(num_graded),
        num_total: parseInt(num_total),
        font_size: parseInt(font_size)
    }, function () {
        // Update status to let user know options were saved.
        var status = document.getElementById('status');
        status.textContent = 'Options saved.';
        setTimeout(function () { status.textContent = ''; }, 750);
    });
}

// Restores select box and checkbox state using the preferences
// stored in chrome.storage.
function restore_options() {
    // Use default value color = 'red' and likesColor = true.
    chrome.storage.sync.get({
        debug: true,
        showBox: true,
        num_graded: 0,
        num_total: 59,
        font_size: 14
    }, function (items) {
        document.getElementById('debug').checked = items.debug;
        document.getElementById('showBox').checked = items.showBox;
        document.getElementById('num_graded').value = items.num_graded;
        document.getElementById('num_total').value = items.num_total;
        document.getElementById('font_size').value = items.font_size;
    });
}
document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('save').addEventListener('click', save_options);