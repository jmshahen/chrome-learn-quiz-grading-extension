// Global Variables that are user controlled and shared accross the extension
var debug = true;
var showBox = true;
var num_graded = 0;
var num_total = 0;
var font_size = 0;
// Global variables not shared accross the extension
var LOG_PREFIX = "[UW Desire2Learn Quiz Extension]";
var manifestData = chrome.runtime.getManifest();

function log() {
    if (debug) {
        // 1. Convert args to a normal array
        var args = Array.prototype.slice.call(arguments);

        // 2. Prepend log prefix log string
        args.unshift(LOG_PREFIX + " ");

        // 3. Pass along arguments to console.log
        console.log.apply(console, args);
    }
}

function actions() {
    if ($('div:contains("Saved successfully")').length == 0) {
        // 1. Register HotKeys
        log("Registering Hotkeys");
        $(document).on('keyup', null, '.', function () {
            log('Clicking the next button');
            $('a[title="Next Record"]')[0].click();
        });

        $(document).on('keyup', null, ',', function () {
            log('Clicking the previous button');
            $('a[title="Previous Record"]')[0].click();
        });

        if ($('input[name=graded]').prop('checked') != true) {
            // 2. Select "Graded (G)" checkbox
            log("Cheking Graded checkbox");
            $('input[name=graded]').prop('checked', true);

            // 3. Scroll to Question 1
            log("Scrolling to Question 1");
            $('html, body').animate({
                scrollTop: $("#z_bp").offset().top
            }, 1000);
        }

        // 4. Increasing Answer's Font Size
        log("Increasing Answer's Font Size");
        $('div.drt').css('font-size', font_size + 'pt');
        $('div.drt p').css('font-size', font_size + 'pt');
        $('div.drt p span').css('font-size', font_size + 'pt');

        // 5. Font Family -- font-family: verdana, sans-serif;
        $('div.drt').css('font-family', 'verdana, sans-serif');
        $('div.drt p').css('font-family', 'verdana, sans-serif');
        $('div.drt p span').css('font-family', 'verdana, sans-serif');
    } else {
        num_graded += 1;
        chrome.storage.sync.set({
            num_graded: num_graded
        }, function () {
            log('Clicking the next button');
            $('a[title="Next Record"]')[0].click();
        });

    }
}

function addPopupBox() {
    log('Adding the popup box to the window');
    
    $('body').append(`
    <style>
     .col-1 {
        text-align: right;
        font-weight: bold;
    }
    div#uwlearnchromeextension {
        background-color: white; 
        text-align:center; 
        width: 220px;
        z-index: 10000;
        position: fixed; 
        top: 70px; 
        right: 60px;
        border-style: solid;
        border-width: 5px;
        border-color: #5D6D7E;
    }
    </style>
    <div id="uwlearnchromeextension">
        <table>
            <tr>
                <td class="col-1">Number of Graded Quizzes:</td>
                <td id="num_graded"></td>
            </tr>
            <tr>
                <td class="col-1">Total Number of Quizzes:</td>
                <td id="num_total"></td>
            </tr>
            <tr>
                <td class="col-1">Number of Quizzes Remaining:</td>
                <td id="num_remaining"></td>
            </tr>
        </table>
        Version <span id="version"></span>
    </div>
    `);

    document.getElementById('num_graded').innerText = num_graded;
    document.getElementById('num_total').innerText = num_total;
    document.getElementById('num_remaining').innerText = parseInt(num_total) - parseInt(num_graded);
    document.getElementById('version').innerText = manifestData.version;

    $('#uwlearnchromeextension').draggable();
}

$(function () {
    chrome.storage.sync.get({
        debug: true,
        showBox: true,
        num_graded: 0,
        num_total: 59,
        font_size: 14
    }, function (items) {
        debug = items.debug;
        showBox = items.showBox;
        num_graded = parseInt(items.num_graded);
        num_total = parseInt(items.num_total);
        font_size = parseInt(items.font_size);

        log("Starting Up Version " + manifestData.version);

        if(showBox) {
            addPopupBox();
        } else {
            log('Skipping the popup box');
        }

        setTimeout(actions, 500);
    });
});