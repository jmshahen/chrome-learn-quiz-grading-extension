var manifestData = chrome.runtime.getManifest();

chrome.storage.sync.get({
    num_graded: 0,
    num_total: 59
}, function (items) {
    num_graded = items.num_graded;
    num_total = items.num_total;

    document.getElementById("go-to-options").addEventListener("click", function () {
        if (chrome.runtime.openOptionsPage) {
            // New way to open options pages, if supported (Chrome 42+).
            chrome.runtime.openOptionsPage();
        } else {
            // Reasonable fallback.
            window.open(chrome.runtime.getURL('options.html'));
        }
    });

    document.getElementById('num_graded').innerText = num_graded;
    document.getElementById('num_total').innerText = num_total;
    document.getElementById('num_remaining').innerText = parseInt(num_total) - parseInt(num_graded);
    document.getElementById('version').innerText = manifestData.version;
});
