# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)

## [Unreleased]

## [1.5] - 2017-11-03
### Added
- Popup draggable box on window to display same stats as the popup windows 
    when you press the extension button
- Popup box can be shown or not shown based on an option
### Changed
- None

## [1.4] - 2017-11-02
### Added
- Keeps track of the total number of quizzes, marked quizzes, and calculates the
    remaining quizzes
- Adding new css to change font-family to verdana and backup font sans-serif
- Font-size is now programmable
### Changed
- Updated the css to include more areas where answers have different font-size

## [1.3] - 2017-11-01
### Added
- README.md file
### Changed
- Updated Options file
- New way of looking up the current version of the extension