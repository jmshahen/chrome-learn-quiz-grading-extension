# Chrome Extension for the Unversity of Waterloo's Desire2Learn Quizzes
This Chrome extension is meant for TAs and Professors of the university of Waterloo.

If you are from a different university and want to make this Chrome extension work for your university, you must edit the `manifest.json` to include or replace the `*://uwaterloo.ca/*` address with your university's web url.

